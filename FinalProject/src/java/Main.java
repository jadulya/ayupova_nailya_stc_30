package java;

import java.sql.*;

public class Main {
    private static final String PG_USER = "postgres";
    private static final String PG_PASSWORD = "123NeLLy123";
    private static final String PG_URL = "jdbc:postgresql://localhost:5432/final_project";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DriverManager.getConnection(PG_URL, PG_USER, PG_PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select name, ip_address from account");

            while (resultSet.next()) {
                System.out.println(resultSet.getString("name") + " "
                        + resultSet.getInt("ip_address"));

            }
        } catch (SQLException throwables) {
            throw new IllegalArgumentException(throwables);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException throwables) {
                    // ignore
                }
            }
        }

    }
}
