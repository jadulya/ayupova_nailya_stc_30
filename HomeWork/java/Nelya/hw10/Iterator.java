package Nelya.hw10;

public interface Iterator {
    int next();
    boolean hasNext();
}
