package Nelya.hw10.LinkedList;

import Nelya.hw10.Iterator;
import Nelya.hw10.List;

public class LinkedList implements List {

    private Node first;
    private Node last;
    private int count;

    @Override
    public int get(int index) {
        if (arrayBoundaries(index)&& first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        throw new IllegalArgumentException("Такого элемента нет");
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            throw new IllegalArgumentException("Элемент не найден");
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node current = this.first;
        if (arrayBoundaries(index)){
            if (index == 0) {
                this.first = current.next;
                count--;
            }
            while (current != null) {
                if ((i + 1) == index) {
                    current.setNext(current.next.next);
                    count--;
                    return;
                } else {
                    current = current.next;
                    i++;
                }
            }
        } else {
            throw new IllegalArgumentException("Индекс вышел за границы массива");
        }
    }


    @Override
    public void insert(int element, int index) {
        Node current = this.first;
        Node tempValue = current.next;
        Node newNode = new Node(element);
        if (arrayBoundaries(index)) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                tempValue = current.next;
            }
            current.next = newNode;
            newNode.next = tempValue;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        } else {
            throw new IllegalArgumentException("Данного индекса нет!");
        }

    }

    @Override
    public void reverse() {
        Node current = this.first;
        Node[] list = new Node[count];
        for (int i = count - 1; i >= 0; i--) {
            list[i] = current;
            current = current.next;
        }

        for (int i = 0; i < count - 1; i++) {
            insert(list[i].value, i);
            count--;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        int i = indexOf(element);
        removeByIndex(i);

    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    private boolean arrayBoundaries(int index) {
        return index < count && index >= 0;
    }

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    private class LinkedListIterator implements Iterator {

        int current = 0;

        @Override
        public int next() {
            int value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return get(current) != -1;
        }
    }
}

