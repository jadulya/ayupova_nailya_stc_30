package Nelya.hw10.ArrayList;

import Nelya.hw10.Iterator;
import Nelya.hw10.List;

public class ArrayList implements List {
    private static final int DEFAULT_SIZE = 10;
    private int data[];
    private int count;

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    @Override
    public int get(int index) {
        if (index < count && index >= 0) {
            return this.data[index];
        }
        throw new IllegalArgumentException("Вышли за пределы массива");
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index <= count) {
            for (int i = index; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
        } else throw new IllegalArgumentException("Индекс вышел за границы массива");

        this.count--;
    }

    @Override
    public void insert(int element, int index) {
        if (index >= 0 && index <= count) {
            count++;
            if (count == data.length - 1) {
                resize();
            }
            for (int i = this.count; i > index; i--) {
                this.data[i] = this.data[i - 1];
            }
            this.data[index] = element;
        } else throw new IllegalArgumentException("Индекс вышел за границы массива");
    }

    @Override
    public void reverse() {
        int value;
        for (int i = 0; i < count / 2; i++) {
            value = data[i];
            data[i] = data[count - 1 - i];
            data[count - 1 - i] = value;
        }
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }


    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);
        int newData[] = new int[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }
}
