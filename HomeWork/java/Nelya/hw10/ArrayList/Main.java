package Nelya.hw10.ArrayList;

import Nelya.hw10.List;

public class Main {

    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }
        list.removeByIndex(6);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
        list.insert(0, 9);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }
}
