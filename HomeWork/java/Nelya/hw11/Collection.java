package Nelya.hw11;

public interface Collection<T> extends Iterable<T> {
    void add(T element);
    boolean contains(T element);
    int size();
    void removeFirst(T element);
}
