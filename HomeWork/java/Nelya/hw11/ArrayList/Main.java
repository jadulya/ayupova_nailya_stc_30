package Nelya.hw11.ArrayList;

import Nelya.hw11.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }
        list.removeByIndex(6);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
        list.insert(0, 9);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }
}


