package Nelya.hw11.ArrayList;

import Nelya.hw11.Iterator;
import Nelya.hw11.List;

public class ArrayList<T> implements List<T> {
    private static final int DEFAULT_SIZE = 10;
    private T data[];
    private int count;

    public ArrayList() {
        this.data = (T[]) new Object[DEFAULT_SIZE];
    }

    @Override
    public T get(int index) {
        if (index < count && index >= 0) {
            return this.data[index];
        }
        throw new IllegalArgumentException("Вышли за пределы массива");
    }


    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index <= count) {
            for (int i = index; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
        } else throw new IllegalArgumentException("Индекс вышел за границы массива");
        this.count--;
    }

    @Override
    public void insert(T element, int index) {
        if (index >= 0 && index <= count) {
            count++;
            if (count == data.length - 1) {
                resize();
            }
            for (int i = this.count; i > index; i--) {
                this.data[i] = this.data[i - 1];
            }
            this.data[index] = element;
        } else throw new IllegalArgumentException("Индекс вышел за границы массива");
    }

    @Override
    public void reverse() {
        T value;
        for (int i = 0; i < count / 2; i++) {
            value = data[i];
            data[i] = data[count - 1 - i];
            data[count - 1 - i] = value;
        }
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }


    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        T newData[] = (T[]) new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public T next() {
            T value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }
}
