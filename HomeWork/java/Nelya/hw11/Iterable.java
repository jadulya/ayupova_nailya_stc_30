package Nelya.hw11;

public interface Iterable<T> {
    Iterator<T> iterator();
}
