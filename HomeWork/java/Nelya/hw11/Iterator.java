package Nelya.hw11;

public interface Iterator<T> {
    T next();
    boolean hasNext();
}
