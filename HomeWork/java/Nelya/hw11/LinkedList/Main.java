package Nelya.hw11.LinkedList;

import Nelya.hw11.Iterator;
import Nelya.hw11.List;

public class Main {

    public static void main(String[] args) {
        int size = 14;
        int temp;
        List<Integer> list = new LinkedList();
        System.out.print("Исходный массив: \n");
        for (int i = 0; i < size; i++) {
            list.add((int) (Math.random() * 10));
            System.out.print(list.get(i) + " ");
        }
        temp = 4;
        System.out.print("\nУдаление по индексу [" + temp + "]: \n");
        list.removeByIndex(temp);
        size--;
        for (int i = 0; i < size; i++) {
            System.out.print(list.get(i) + " ");
        }
        temp = 3;
        System.out.print("\nУдаление элемента " + temp + ": \n");
        if (list.contains(temp)) {
            size--;
            list.removeFirst(temp);
        }
        for (int i = 0; i < size; i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        Iterator<Integer> iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        list.reverse();
        iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

    }
}
