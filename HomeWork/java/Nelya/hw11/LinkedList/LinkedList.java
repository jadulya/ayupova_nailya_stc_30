package Nelya.hw11.LinkedList;

import Nelya.hw11.Iterator;
import Nelya.hw11.List;

public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;
    private int count;

    @Override
    public T get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<T> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;
    }

    @Override
    public int indexOf(T element) {
        int i = 0;
        Node<T> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            System.err.println("Элемент не найден");
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node<T> current = this.first;
        if (index == 0) {
            this.first = current.next;
            count--;
        }
        while (current != null) {
            if ((i + 1) == index) {
                current.setNext(current.next.next);
                count--;
                return;
            } else {
                current = current.next;
                i++;
            }
        }
    }

    @Override
    public void insert(T element, int index) {
        Node<T> current = this.first;
        Node<T> current1 = current.next;
        Node<T> newNode = new Node<T>(element);
        if (index <= count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = newNode;
            newNode.next = current1;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        } else {
            System.out.println("Данного индекса нет!");
        }

    }

    @Override
    public void reverse() {
        Node<T> current = this.first;
        Node<T>[] list = new Node[count];
        for (int i = count - 1; i >= 0; i--) {
            list[i] = current;
            current = current.next;
        }

        for (int i = 0; i < count - 1; i++) {
            insert(list[i].value, i);
            count--;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<T>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(T element) {
        int i = indexOf(element);
        removeByIndex(i);

    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }

    private class LinkedListIterator implements Iterator<T> {

        int current = 0;

        @Override
        public T next() {
            T value = (T) get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return get(current) != null;
        }
    }
}

