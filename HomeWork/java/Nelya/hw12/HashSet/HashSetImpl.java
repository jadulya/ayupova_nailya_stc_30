package Nelya.hw12.HashSet;

import Nelya.hw12.HashMap.HashMapImpl;
import Nelya.hw12.Set;


public class HashSetImpl<V> implements Set<V> {
    private static final Object PRESENT = new Object();
    private final transient HashMapImpl<V, Object> map;

    public HashSetImpl() {
        map = new HashMapImpl<V, Object>();
    }

    @Override
    public void add(V value) {
        map.put(value, PRESENT);
    }

    @Override
    public boolean contains(V value) {
        return map.get(value) != null;
    }
}
