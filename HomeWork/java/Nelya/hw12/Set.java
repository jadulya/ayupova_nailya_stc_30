package Nelya.hw12;


// коллекция, которая гарантирует уникальность своих значений
public interface Set<V> {
    void add(V value);
    boolean contains(V value);

}
