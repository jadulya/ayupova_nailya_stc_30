package Nelya.hw12.HashMap;

import Nelya.hw12.Map;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];


    // TODO не плохо бы поработать над декомпозицией (повыделять дублированынй код и блоки кода прдеставляющие собой самостоятельную логику в приватные методы)
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            MapEntry<K, V> current = entries[index];
            if (key.equals(current.key)) { // TODO у тебя сейчас ключ сравнивается по equals только для первого элемента в бакете
                current.value = value;
                while (current.next != null) {
                    current = current.next;
                }
                current.next = newMapEntry;
            }
        }
    }

    @Override
    public V get(K key) {

        int index = key.hashCode() & (entries.length - 1);
        if (key.equals(entries[index].value)) {
            return entries[index].value;
        }
        throw new IllegalArgumentException("Данного элемента не существует"); // TODO тут кидать исключение больновато (прийдется все вызовы метода get заворачивать в try/catch),  лучше возвращать null
    }

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
