package Nelya.hw12.HashMap;

import Nelya.hw12.Map;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();

        map.put("Марсель", 26);
        map.put("Денис", 30);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Катерина", 23);
        map.put("Марсель", 29);

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Катерина"));

        Set<String> set = new HashSet<>();

        map.put("123", 123);
        map.put("123", 456);

//
//        map.put("Марсель", 26);
//        map.put("Денис", 30);
//        map.put("Илья", 28);
//        map.put("Неля", 18);
//        map.put("Катерина", 23);
//        map.put("Марсель", 29);
//
//        System.out.println(map.get("Марсель"));
//        System.out.println(map.get("Денис"));
//        System.out.println(map.get("Илья"));
//        System.out.println(map.get("Неля"));
//        System.out.println(map.get("Катерина"));
    }
}
