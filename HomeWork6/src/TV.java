public class TV {
    public String name;
    Channel channel = new Channel();

    public TV(String name) {
        this.name = name;
    }

    public void findChannel(int activeChannel, TV myTv) {
        if (activeChannel >= 1 && activeChannel <= 4) {
            channel.selectActiveChannel(activeChannel, myTv);
        } else {
            System.out.println("Канал не найден. В ваш пакет домашнего телевидения входит только 5 каналов (1-5)");
        }
    }

    public void showProgram(String program, String activeChannel) {
        System.out.println("Приятного просмотра программы '" + program + "' на телеканале '" + activeChannel + "'");
    }
}