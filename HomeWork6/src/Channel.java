public class Channel {
    Program program = new Program();
    String[] nameOfChannels = {"Первый", "Россия1", "НТВ", "ТНТ", "СТС"};

    public void selectActiveChannel(int activeChannel, TV myTv) {
        String selectedChannel = nameOfChannels[activeChannel - 1];
        program.selectProgram(selectedChannel, myTv);
    }
}
