public class RemoteController {
    public String name;

    public RemoteController(String name) {
        this.name = name;
    }

    public void switchChannel(int activeChannel, TV myTv) {
        myTv.findChannel(activeChannel, myTv);
    }
}
