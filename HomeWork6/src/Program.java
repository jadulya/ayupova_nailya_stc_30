public class Program {

    public void selectProgram(String selectedChannel, TV myTv) {
        int random = (int) (Math.random() * 4);
        if (selectedChannel.equals("Первый")) {
            String[] nameOfProgram = {"Время", "Пусть говорят", "Давай поженимся", "КВН"};
            String randomProgram = nameOfProgram[random];
            myTv.showProgram(randomProgram, selectedChannel);
        }
        if (selectedChannel.equals("Россия1")) {
            String[] nameOfProgram = {"Вести", "Доярка из Хацапетовки", "тайны следствия", "Спокойной ночи, малыши"};
            String randomProgram = nameOfProgram[random];
            myTv.showProgram(randomProgram, selectedChannel);
        }
        if (selectedChannel.equals("НТВ")) {
            String[] nameOfProgram = {"Мухтар", "Секрет на миллион", "Квартирный вопрос", "Едим дома"};
            String randomProgram = nameOfProgram[random];
            myTv.showProgram(randomProgram, selectedChannel);
        }
        if (selectedChannel.equals("ТНТ")) {
            String[] nameOfProgram = {"ДОМ-2", "Ольга", "Comedy Club", "Битва экстрасенсов"};
            String randomProgram = nameOfProgram[random];
            myTv.showProgram(randomProgram, selectedChannel);
        }
        if (selectedChannel.equals("СТС")) {
            String[] nameOfProgram = {"Галилео", "Ранетки", "Не родись красивой", "Кухня"};
            String randomProgram = nameOfProgram[random];
            myTv.showProgram(randomProgram, selectedChannel);
        }
    }
}
