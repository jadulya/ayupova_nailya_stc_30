import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        RemoteController controller = new RemoteController("Controller");
        TV myTv = new TV("Panasonic");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Телевизор включен. Выберите канал от 1 до 5");
        try {
            int activeChannel = scanner.nextInt();
            controller.switchChannel(activeChannel, myTv);
        } catch (Exception e) {
            System.out.println("Неверный формат номера канала");
        }
    }
}