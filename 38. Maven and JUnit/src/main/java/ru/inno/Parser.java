package ru.inno;

/**
 * 13.12.2020
 * 38. Maven and JUnit
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Parser {
    /**
     * Преобразует строку в число.
     * @param string входная строка, которая содержит число: {1234, -1234, +1234}
     * @return int-представление числа
     * @throws IllegalArgumentException в случае, когда строка имеет неверный формат
     */
    int parse(String string);
}
