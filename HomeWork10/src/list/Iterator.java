package list;

// PRODUCT
public interface Iterator {
    // возвращает следующий элемент
    int next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
