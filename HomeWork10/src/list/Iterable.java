package list;// общий интерфейс, он говорит о том, что можно получить
// объект-итератор

// CREATOR
public interface Iterable {
    // FactoryMethod()
    Iterator iterator();
}
