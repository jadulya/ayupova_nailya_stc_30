package examples;

public class Main {

    public static void main(String[] args) {
        System.out.println(Ship.getQuantityOfCreatedShips());

        Ship ship = new Ship(10, "1234");
        ship.swim();

        System.out.println(Ship.getQuantityOfCreatedShips());

        Main main = new Main();
        System.out.println(main.sum(1, 2));

        System.out.println(sumStatic(1, 2));
    }

    public int sum(int p1, int p2) {
        return p1 + p2;
    }

    public static int sumStatic(int p1, int p2) {
        return p1 + p2;
    }

}
