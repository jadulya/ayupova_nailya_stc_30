package examples;

public class Ship {

    private int size;
    private String name;

    private static int quantityOfCreatedShips = 0;

    public Ship() {
        incrementQuantityOfShips();
    }

    public Ship(int size, String name) {
        this.size = size;
        this.name = name;
        incrementQuantityOfShips();
    }

    @Override
    public String toString() {
        return "Ship{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }

    private void incrementQuantityOfShips(){
        quantityOfCreatedShips++;
    }

    public static int getQuantityOfCreatedShips() {
        return quantityOfCreatedShips;
    }

    public void swim() {
        System.out.println("chuh chuh ");
    }
}
