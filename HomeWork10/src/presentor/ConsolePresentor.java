package presentor;

public class ConsolePresentor implements Presentor {

    public void present(String content) {
        System.out.println(content.toLowerCase());
    }
}
