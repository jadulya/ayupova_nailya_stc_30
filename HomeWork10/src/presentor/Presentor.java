package presentor;

public interface Presentor {

    void present(String content);
}
