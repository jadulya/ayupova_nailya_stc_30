package presentor;

public class Program {

    private Logic logic;
    private Presentor presentor;

    public Program(Logic logic, Presentor presentor) {
        this.logic = logic;
        this.presentor = presentor;
    }

    public void work() {
        String result = logic.doSomeAction();
        presentor.present(result);
    }
}
