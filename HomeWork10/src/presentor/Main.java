package presentor;

public class Main {

    public static void main(String[] args) {
        Program program = new Program(new Logic(), new ConsolePresentor());
        program.work();
    }
}
