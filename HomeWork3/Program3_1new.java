import java.util.Arrays;
import java.util.Scanner;

class Program3_1new {
    public static void main(String[] args) {
        System.out.println("Enter the size of the array ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Enter array elements ");
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Выберите метод, который хотите применить к введенному массиву. Введите номер метода.\n" +
                "1. Выводит сумму массива\n2. Выводит среднее арифмитическое массива\n" +
                "3. Выполняет преобразование массива в число\n4. Выполняет разворот массива\n" +
                "5. Меняет местами максимальный и минимальный элементы массива\n" +
                "6. Выполняет сортировку массива методом пузырька");
        Scanner scan = new Scanner(System.in);
        int numberOfMethod = scan.nextInt();
        switch (numberOfMethod) {
            case (1):
                System.out.println("Sum of array elements = " + sumOfArray(array));
                break;
            case (2):
                System.out.println("Average of array = " + (double) sumOfArray(array) / n);
                break;
            case (3):
                System.out.println("Convert array to number: " + convertArrayToNum(array));
                break;
            case (4):
                backwardsArray(array);
                break;
            case (5):
                changeMinMaxValueOfArray(array);
                break;
            case (6):
                System.out.println("Bubble sort of array: " + Arrays.toString(bubbleSort(array)));
                break;
            default:
                System.out.println("Вы ввели неверное значение");
                break;
        }
    }

    public static int sumOfArray(int[] array) {
        int arraySum = 0;
        for (int j : array) {
            arraySum += j;
        }
        return arraySum;
    }

    public static void backwardsArray(int[] array) {
        System.out.print("Backwards array: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[array.length - i - 1] + " ");
        }
        System.out.println();
    }

    public static void changeMinMaxValueOfArray(int[] array) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int minIndex = 0;
        int maxIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                minIndex = i;
            }
            if (array[i] > max) {
                max = array[i];
                maxIndex = i;
            }
        }
        array[minIndex] += array[maxIndex] - (array[maxIndex] = array[minIndex]);
        System.out.println("Changed of place min and max value of array: " + (Arrays.toString(array)));
    }

    public static int[] bubbleSort(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    array[i] += array[i + 1] - (array[i + 1] = array[i]);
                }
            }
        }
        return array;
    }

    public static int convertArrayToNum(int[] array) {
        int number = array[0];
        int temp;
        for (int i = 1; i < array.length; i++) {
            number *= 10;
            temp = array[i];
            while (temp / 10 != 0) {
                number *= 10;
                temp /= 10;
            }
            number += Math.abs(array[i]);
        }
        return number;
    }
}

