// сортировка выбором + бинарный поиск

import java.util.Arrays;
import java.util.Scanner;

public class Program4_2 {
    public static void main(String[] args) {

        int[] array = {2, 4, 10, 11, 12, 13};
        System.out.println("Исходный массив " + Arrays.toString(array) + "\nВведите искомое число");
        Scanner scanner = new Scanner(System.in);
        int numberForSearch = scanner.nextInt();

        System.out.println("Индекс искомого числа: " + binSearch(array, numberForSearch));
    }

    public static int binSearch(int[] array, int number) {
        int from = 0;
        int to = array.length - 1;
        return binSearch(array, number, from, to);
    }

    private static int binSearch(int[] array, int number, int from, int to) {
        if (from <= to) {
            int middle = from + (to - from) / 2;
            if (array[middle] == number) {
                return middle;
            } else if (array[middle] > number) {
                return binSearch(array, number, from, middle - 1);
            } else if (array[middle] < number) {
                return binSearch(array, number, middle + 1, to);
            }
        }
        return -1;
    }
}