class Program4_1 {
    public static long fib(long[] f, int n) {
        System.out.println("--> fib(" + n + ")");
        if (n == 0) {
            System.out.println("<-- fib(" + n + ") = 0");
            return f[n] = 0;
        }
        if (n == 1) {
            System.out.println("<-- fib(" + n + ") = 1");
            return f[n] = 1;
        }
        if (f[n] > 0) {
          return f[n];
        }
        f[n] = fib(f, n - 1) + fib(f, n - 2);
        System.out.println("<-- fib(" + n + ") = " + f[n]);
        return f[n];
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println(fib(new long[n + 1], n));
    }
}
