public class Square extends GeometricFigure {
    protected double sideA;

    public Square(double oX, double oY, double sideA) {
        super(oX, oY);
        this.sideA = sideA;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    @Override
    public void calculateArea() {
        setArea(this.sideA * this.sideA);
    }

    @Override
    public void calculatePerimeter() {
        setPerimeter(4 * this.sideA);
    }

    @Override
    public void toScale(double value) {
        sideA = value * sideA;
    }

}
