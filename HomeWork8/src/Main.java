public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(0,1,2);
        circle.calculateArea();
        circle.calculatePerimeter();
        System.out.println("Площадь круга = " + circle.getArea());
        System.out.println("Периметр круга = " + circle.getPerimeter());
        circle.toScale(3);
        System.out.println("Масштабируемое значение радиуса = " + circle.getRadius());
        circle.toRelocate(1,2);
        System.out.println("oX = " + circle.getoX() + ", oY =  " + circle.getoY() + "\n");

        Ellipse ellipse = new Ellipse(1, 2, 5, 1);
        ellipse.calculateArea();
        ellipse.calculatePerimeter();
        System.out.println("Площадь эллипса = " + ellipse.getArea());
        System.out.println("Периметр эллипса = " + ellipse.getPerimeter());
        ellipse.toScale(2);
        System.out.println("Масштабируемое значение большой полуоси = " + ellipse.getRadius() +
                "\nМасштабируемое значение малой полуоси = " + ellipse.getSmallRadius());
        ellipse.toRelocate(2, 1);
        System.out.println("oX = " + ellipse.getoX() + ", oY = " + ellipse.getoY() + "\n");

        Square square = new Square(2,3.5,1);
        square.calculateArea();
        square.calculatePerimeter();
        System.out.println("Площадь квадрата = " + square.getArea());
        System.out.println("Периметр квадрата = " + square.getPerimeter());
        square.toScale(2.3);
        System.out.println("Масштабируемой значение стороны А = " + square.getSideA());
        square.toRelocate(1.5, 2.3);
        System.out.println("oX = " + square.getoX() + ", oY = " + square.getoY() + "\n");

        Rectangle rectangle = new Rectangle(4.2, 3, 7, 7);
        rectangle.calculateArea();
        rectangle.calculatePerimeter();
        System.out.println("Площадь прямоугольника = " + rectangle.getArea());
        System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter());
        rectangle.toScale(1.3);
        System.out.println("Масштабируемое значение стороны А = " + rectangle.getSideA() +
                ", масштабируемое значение стороны В = " + rectangle.getSideB());
        rectangle.toRelocate(1.7, -7);
        System.out.println("oX = " + rectangle.getoX() + ", oY = " + rectangle.getoY());
    }
}
