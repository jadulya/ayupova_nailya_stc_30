import static java.lang.Math.*;

public class Ellipse extends Circle {
    private double smallRadius;

    public Ellipse(double oX, double oY, double radius, double smallRadius) {
        super(oX, oY, radius);
        this.smallRadius = smallRadius;
    }

    public double getSmallRadius() {
        return smallRadius;
    }

    @Override
    public void calculateArea() {
        setArea(PI * this.radius * this.smallRadius);
    }

    @Override
    public void calculatePerimeter() {
        setPerimeter(2 * PI * sqrt((pow(this.radius, 2) + pow(this.smallRadius, 2)) / 2));
    }

    @Override
    public void toScale(double value) {
        setRadius(value * radius);
        smallRadius *= value;
    }

}
