public class Rectangle extends Square {
    protected double sideB;

    public Rectangle(double oX, double oY, double sideA, double sideB) {
        super(oX, oY, sideA);
        this.sideB = sideB;
    }

    public double getSideB() {
        return sideB;
    }

    @Override
    public void calculateArea() {
        setArea(this.sideA * this.sideB);
    }

    @Override
    public void calculatePerimeter() {
        setPerimeter(2 * (this.sideA + this.sideB));
    }

    @Override
    public void toScale(double value) {
        setSideA(value * this.sideA);
        sideB *= value;
    }

}
