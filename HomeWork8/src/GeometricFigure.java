public abstract class GeometricFigure implements Scalable{//, Relocatable {
    protected double area;
    protected double perimeter;
    protected double oX;
    protected double oY;

    public GeometricFigure(double oX, double oY) {
        this.oX = oX;
        this.oY = oY;
    }

    public double getoX() {
        return oX;
    }

    public void setoX(double oX) {
        this.oX = oX;
    }

    public double getoY() {
        return oY;
    }

    public void setoY(double oY) {
        this.oY = oY;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    public abstract void calculateArea();

    public abstract void calculatePerimeter();

    public void toRelocate(double valueOX, double valueOY) {
        setoX(oX += valueOX);
        setoY(oY += valueOY);}
}
