import static java.lang.Math.*;

public class Circle extends GeometricFigure {
    protected double radius;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public Circle(double oX, double oY, double radius) {
        super(oX, oY);
        this.radius = radius;
    }

    @Override
    public void calculateArea() {
        setArea(PI * pow(this.radius, 2));
    }

    @Override
    public void calculatePerimeter() {
        setPerimeter(2 * PI * this.radius);
    }

    @Override
    public void toScale(double value) {
        radius = value * radius;
    }

}
