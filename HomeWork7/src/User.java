public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public static class Builder {
        private User newUser;

        public Builder() {
            newUser = new User();
        }
        public Builder firstName (String firstName){
            newUser.firstName = firstName;
            System.out.println("Firstname: " + firstName);
            return this;
        }
        public Builder lastName (String lastName){
            newUser.lastName = lastName;
            System.out.println("Lastname: " + lastName);
            return this;
        }
        public Builder age (int age){
            newUser.age = age;
            System.out.println("Age: " + age);
            return this;
        }
        public Builder isWorker (boolean isWorker){
            newUser.isWorker = isWorker;
            System.out.println("Is worker: " + isWorker);
            return this;
        }
        public User build(){
            return newUser;
        }
    }
}
