// список на основе массива
// очень быстрое получение элемента по индексу - O(1)
// быстрое добавление в конец - О(1)
// долгое удаление - О(N)
// требует расширения при переполнении, тогда добавление(вставка) - O(N)
// органиченный размер MAX_INT
// memory - N * VALUE + K * LINK, например, если в массиве 100 ячеек, заполненно 10, то memory - 10 * VALUE + 100 * LINK

// CONCRETE CREATOR
public class ArrayList<T> implements List<T> {
    // контстанта для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    // поле, представляющее собой массив, в котором мы храним элементы
    private T data[];
    // количество элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = (T[])new Object[DEFAULT_SIZE];
    }

    @Override
    public T get(int index) { // TODO нужно добавить провку индексов
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");// TODO throw new IllegalArgumentException();
        return null;
    }


    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) { // TODO ссылочные типы сравниваются через equals
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        for (int i = index; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }

    @Override
    public void insert(T element, int index) {
        count++;
        if (count == data.length - 1) {
            resize();
        }
        for (int i = this.count; i > index; i--) {
            this.data[i] = this.data[i - 1];
        }
        this.data[index] = element;
    }

    @Override
    public void reverse() {
        T value;
        for (int i = 0; i < count / 2; i++) {
            value = data[i];
            data[i] = data[count - 1 - i];
            data[count - 1 - i] = value;
        }
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    /**
     * 10 -> 100
     * 15
     * <p>
     * 100 -> 110
     * 150
     * <p>
     * n -> n * 1.5
     */
    private void resize() {
        int oldLength = this.data.length;
        //  127(10) -> 1111111(2)
        //  1111111 >> 1 -> 0111111 -> 63
        // N >> K -> N / 2^K
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        T newData[] = (T[])new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
    }

    // реализация метода получения объекта
    @Override
    public Iterator<T> iterator() {
        // CONCRETE PRODUCT
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public T next() {
            T value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }
}
