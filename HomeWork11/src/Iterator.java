// PRODUCT
public interface Iterator<T> {
    // возвращает следующий элемент
    T next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
