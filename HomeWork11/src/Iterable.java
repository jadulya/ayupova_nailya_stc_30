// общий интерфейс, он говорит о том, что можно получить
// объект-итератор

// CREATOR
public interface Iterable<T> {
    // FactoryMethod()
    Iterator<T> iterator();
}
