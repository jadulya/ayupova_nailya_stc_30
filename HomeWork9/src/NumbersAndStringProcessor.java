public class NumbersAndStringProcessor  {
    private String[] arrayString;
    private int[] arrayInt;

    public NumbersAndStringProcessor(int[] arrayInt) {
        this.arrayInt = arrayInt;
    }
    public NumbersAndStringProcessor(String[] arrayString) {
        this.arrayString = arrayString;
    }
    public String[] process(StringProcess process) {
        String[] newString = new String[arrayString.length];
        for (int i = 0; i < arrayString.length; i++) {
            newString[i] = process.process(arrayString[i]);
        }
        return newString;
    }

    public int[] process(NumberProcess process) {
        int[] newInt = new int[arrayInt.length];
        for (int i = 0; i < arrayInt.length; i++) {
            newInt[i] = process.process(arrayInt[i]);
        }
        return newInt;
    }




}
