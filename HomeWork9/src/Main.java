import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arrayInt = {230451, 78039, 4203405, 12090803};
        String[] arrayString = {"Hi11", "he110", "g00dbye", "John"};
        NumberProcess deleteZero = number -> {
            int temp = number;
            int[] array = numberToArray(temp);
            int count = array.length;
            for (int i = 0; i < count; i++) {
                if (array[i] == 0) {
                    for (int j = i; j < count - 1; j++) {
                        array[j] = array[j + 1];
                    }
                    count--;
                    i--;
                }
            }
            array = Arrays.copyOf(array, count);
            return arrayToNumber(array);
        };

        NumberProcess oddToEven = number -> {
            int temp = number;
            int[] array = numberToArray(temp);
            for (int i = 0; i < array.length; i++) {
                if (array[i] % 2 != 0) {
                    array[i]--;
                }
            }
            return arrayToNumber(array);
        };

        NumberProcess reverse = number -> {
            int temp = number;
            int[] array = numberToArray(temp);
            for (int i = 0; i < array.length/2; i++) {
                    array[i]+=array[array.length -i -1] - (array[array.length -i -1] = array[i]);
                }
            return arrayToNumber(array);
        };

        StringProcess reverseString = value -> {
            String temp = value;
            char[] array = temp.toCharArray();
            for (int i = 0; i < array.length/2; i++) {
                array[i]+=array[array.length -i -1] - (array[array.length -i -1] = array[i]);
            }
            return String.valueOf(array);
        };

        StringProcess deleteDigit = process -> process.replaceAll("[0-9]", "");

        StringProcess upperCase = process -> process.toUpperCase();

        NumbersAndStringProcessor numProcess = new NumbersAndStringProcessor(arrayInt);
        NumbersAndStringProcessor stringProcess = new NumbersAndStringProcessor(arrayString);
        int[] result = numProcess.process(deleteZero);
        System.out.println(Arrays.toString(result));
        result = numProcess.process(oddToEven);
        System.out.println(Arrays.toString(result));
        result = numProcess.process(reverse);
        System.out.println(Arrays.toString(result));

        String[] string = stringProcess.process(reverseString);
        System.out.println(Arrays.toString(string));
        string = stringProcess.process(upperCase);
        System.out.println(Arrays.toString(string));
        string = stringProcess.process(deleteDigit);
        System.out.println(Arrays.toString(string));

    }

    private static int arrayToNumber(int[] array) {
        int number = 0;
        for (int i = 0; i < array.length; i++) {
            number = number * 10 + array[i];
        }
        return number;

    }

    private static int[] numberToArray(int number) {
        int temp = number;
        int count = 0;
        int[] array;
        while (temp != 0) {
            temp /= 10;
            count++;
        }
        array = new int[count];
        temp = number;
        for (int i = array.length - 1; i >= 0; i--) {
            array[i] = temp % 10;
            temp /= 10;
        }
        return array;
    }
}

