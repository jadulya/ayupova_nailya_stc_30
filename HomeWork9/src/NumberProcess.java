@FunctionalInterface
public interface NumberProcess {
    int process(int number);
}
